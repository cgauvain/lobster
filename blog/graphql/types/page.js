import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLInputObjectType,
  GraphQLEnumType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean,
  GraphQLID,
  GraphQLInt
} from "graphql";
import { connectionDefinitions } from "graphql-relay";

let PageType = new GraphQLObjectType({
  name: "Page",
  description:
    "This represent a facebook page. It could be a concert hall or a collective",
  fields: () => ({
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    fb_id: {
      type: new GraphQLNonNull(GraphQLString)
    },
    token: {
      type: new GraphQLNonNull(GraphQLString)
    },
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    description: {
        type: GraphQLString
    },
    default_style: {
        type: new GraphQLEnumType({
            name: 'default_style',
            // Names have to be international, so we can't use spaces or /
            values: {
                'Electro': { value: 'electro'},
                'HIP_HOP': {value: 'hip_hop'},
                'POP_ROCK': {value: 'pop_rock'},
                'Jazz': {value: 'jazz'},
            }
        })
    },
    /*default_style: {
        type: new GraphQLEnumType({
            name: 'default_style',
            /*ArrPerson.forEach(function(key,value){
              console.log(key);
              console.log(value)
            })* /
            values: {
                Electro: { value: 'electro' },
                //hip_hop: { value: 'hip_hop' },
                //pop_rock: { value: 'pop_rock' },
                Jazz: { value: 'jazz' }
            }
        })
    },*/
    is_active: {
        type: GraphQLBoolean
    },
    create_date: {
        type: GraphQLString
    },
    update_date: {
        type: GraphQLString
    }
  })
});

let PageInputType = new GraphQLInputObjectType({
  name: "PageInput",
  fields: () => ({
    _id: {
      type: GraphQLID
    },
    clientMutationId: {
        type: GraphQLString
    },
    fb_id: {
      type: GraphQLString
    },
    token: {
      type: GraphQLString
    },
    name: {
      type: GraphQLString
    },
    description: {
        type: GraphQLString
    },
    default_style: {
        type: GraphQLString
    },
    is_active: {
        type: GraphQLBoolean
    }
  })
});

let PageResultType = new GraphQLObjectType({
  name: "PageResult",
  fields: () => ({
    errors: {
      type: new GraphQLList(GraphQLString)
    },
    page: {
      type: PageType
    }
  })
});

let PageDeleteType = new GraphQLObjectType({
  name: "PageDelete",
  fields: () => ({
    deleted: {
      type: GraphQLBoolean
    },
    page: {
      type: PageType
    }
  })
});

const {
  connectionType: PageListConnection,
  edgeType: PageListEdge
} = connectionDefinitions({
  name: "PageList",
  nodeType: PageType,
  connectionFields: () => ({
    filteredCount: {
      type: GraphQLInt,
      resolve: connection => connection.filteredCount
    },
    totalCount: {
      type: GraphQLInt,
      resolve: connection => connection.totalCount
    }
  })
});

module.exports = {
  PageListConnection: PageListConnection,
  PageType: PageType,
  PageInputType: PageInputType,
  PageResultType: PageResultType,
  PageDeleteType: PageDeleteType
};
