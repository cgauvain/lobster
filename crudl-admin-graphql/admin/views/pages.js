import React from "react";
import SplitDateTimeField from "../fields/SplitDateTimeField";

import { createResourceConnector } from "../connectors";

const PageFields = '_id, fb_id, token, name, ' +
                   'description, default_style, '+
                   'is_active, create_date, update_date';
const pages = createResourceConnector("pages", PageFields);

//-------------------------------------------------------------------
var listView = {
  path: "pages",
  title: "Pages",
  actions: {
    list: function(req) {
      return pages.read(req);
    }
  }
};

listView.fields = [
  {
    name: "name",
    label: "Name",
    sortable: true,
    sorted: "ascending",
    main: true
  },
  {
    name: "fb_id",
    label: "ID FB",
    sortable: true,
  },
  {
    name: "token",
    label: "Token FB",
    sortable: true,
  },
  {
    name: "default_style",
    label: "Default style",
    sortable: true,
  },
  {
    name: "is_active",
    label: "Active",
    render: "boolean",
    sortable: true,
  },
  {
      name: 'create_date',
      label: 'Creation date',
      sortable: true,
  }
];

listView.filters = {
    fields: [
        {
            name: 'search',
            label: 'Search',
            field: 'Search',
            helpText: 'Name, FB ID or Token',
        },
        {
            name: 'default_style',
            label: 'Default style',
            field: 'Select',
            options: [
                {value: 'electro', label: 'Electro'},
                {value: 'hip_hop', label: 'HIP HOP'},
                {value: 'pop_rock', label: 'POP/ROCK'},
                {value: 'jazz', label: 'JAZZ'},
            ],
        },
        {
            name: 'is_active',
            label: 'Active',
            field: 'Select',
            options: [
                {value: 'true', label: 'True'},
                {value: 'false', label: 'False'}
            ],
            helpText: 'Note: We use Select in order to distinguish false and none.',
        },
        {
            name: 'date_gt',
            label: 'Created after',
            field: 'Date',
            /* simple date validation (please note that this is just a showcase,
            we know that it does not check for real dates) */
            validate: (value, allValues) => {
                const dateReg = /^\d{4}-\d{2}-\d{2}$/
                if (value && !value.match(dateReg)) {
                    return 'Please enter a date (YYYY-MM-DD).'
                }
            }
        },
        {
            name: 'search_description',
            label: 'Search (description)',
            field: 'Search',
        },
    ]
};

//-------------------------------------------------------------------
var changeView = {
  path: "pages/:_id",
  title: "Page",
  actions: {
    get: function(req) {
      return pages(crudl.path._id).read(req);
    },
    save: function(req) {
      return pages(crudl.path._id).update(req);
    }
  },
  denormalize: function (data) {
      /* prevent unknown field ... with query */
      delete(data.update_date)
      delete(data.create_date)
      return data
  },
};

changeView.fieldsets = [
  {
    fields: [
      {
          name: '_id',
          hidden: true,
      },
      {
        name: "fb_id",
        label: "ID FB",
        field: "String",
        required: true
      },
      {
        name: "token",
        label: "Token FB",
        field: "String",
        required: true
      }
    ]
  },
  {
    title: 'Content',
    expanded: true,
    fields: [
      {
        name: "name",
        label: "Name",
        field: "String",
        required: true
      },
      {
        name: "description",
        field: "Textarea"
      }
    ]
  },
  {
      title: 'Internal',
      expanded: true,
      fields: [
          {
            name: "is_active",
            label: "Active",
            field: "Checkbox",
            initialValue: true,
            helpText:
              "If the page is marked has inactive, we will not retrieve its events."
          },
          {
            name: "default_style",
            label: "Default style",
            field: "Select",
            required: false,
            options: [
                {value: 'electro', label: 'Electro'},
                {value: 'hip_hop', label: 'HIP HOP'},
                {value: 'pop_rock', label: "POP/ROCK"},
                {value: 'jazz', label: 'JAZZ'},
            ],
            helpText: 'Note: Choose a default style, if possible, for the page.',
          },
          {
              name: 'create_date',
              label: 'Date (Create)',
              field: 'Datetime',
              readOnly: true
          },
          {
              name: 'update_date',
              label: 'Date (Update)',
              field: 'Datetime',
              readOnly: true
          }
      ]
  }
];

//-------------------------------------------------------------------
var addView = {
  path: "pages/new",
  title: "New Page Entry",
  fieldsets: changeView.fieldsets,
  actions: {
    add: function(req) {
      return pages.create(req);
    }
  }
};

module.exports = {
  listView,
  changeView,
  addView
};
